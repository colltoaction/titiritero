﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Fiuba.Algo3.Titiritero.Vista;

namespace Fiuba.Algo3.Ejemplos.Pelotas
{
    public partial class Form1 : Form
    {
        EjemploPelotas ejemplo;
        public Form1()
        {
            InitializeComponent();
            this.ejemplo = new EjemploPelotas(this.panel1);
        }

        private void botonComenzar_Click(object sender, EventArgs e)
        {
            ejemplo.Comenzar();
        }

        private void botonDetener_Click(object sender, EventArgs e)
        {
            ejemplo.Detener();
        }
    }
}
