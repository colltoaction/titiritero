﻿namespace Fiuba.Algo3.Ejemplos.Pelotas
{
    using Fiuba.Algo3.Titiritero.Vista;

    public class VistaMesa : Cuadrado
    {
        public VistaMesa(Mesa unaMesa) :base(unaMesa.Ancho, unaMesa.Alto, System.Drawing.Color.Blue)
        {
            this.Posicionable = unaMesa;
        }
    }
}
