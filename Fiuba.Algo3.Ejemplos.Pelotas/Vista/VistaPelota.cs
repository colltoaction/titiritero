﻿namespace Fiuba.Algo3.Ejemplos.Pelotas
{
    using Fiuba.Algo3.Titiritero.Vista;

    class VistaPelota : Circulo
    {
        public VistaPelota(Pelota unaPelota)
            : base(20, System.Drawing.Color.Red)
        {
            this.Posicionable = unaPelota;
        }
    }
}
