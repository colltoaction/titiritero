﻿namespace Fiuba.Algo3.Ejemplos.Pelotas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.botonComenzar = new System.Windows.Forms.Button();
            this.panel1 = new Fiuba.Algo3.Titiritero.Vista.Panel();
            this.botonDetener = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // botonComenzar
            // 
            this.botonComenzar.Location = new System.Drawing.Point(13, 13);
            this.botonComenzar.Name = "botonComenzar";
            this.botonComenzar.Size = new System.Drawing.Size(75, 23);
            this.botonComenzar.TabIndex = 1;
            this.botonComenzar.Text = "Comenzar";
            this.botonComenzar.UseVisualStyleBackColor = true;
            this.botonComenzar.Click += new System.EventHandler(this.botonComenzar_Click);
            // 
            // panel1
            // 
            this.panel1.Location = new System.Drawing.Point(12, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 200);
            this.panel1.TabIndex = 0;
            // 
            // botonDetener
            // 
            this.botonDetener.Location = new System.Drawing.Point(137, 13);
            this.botonDetener.Name = "botonDetener";
            this.botonDetener.Size = new System.Drawing.Size(75, 23);
            this.botonDetener.TabIndex = 2;
            this.botonDetener.Text = "Detener";
            this.botonDetener.UseVisualStyleBackColor = true;
            this.botonDetener.Click += new System.EventHandler(this.botonDetener_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(226, 278);
            this.Controls.Add(this.botonDetener);
            this.Controls.Add(this.botonComenzar);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Ejemplo pelotas";
            this.ResumeLayout(false);

        }

        #endregion

        private Titiritero.Vista.Panel panel1;
        private System.Windows.Forms.Button botonComenzar;
        private System.Windows.Forms.Button botonDetener;
    }
}

