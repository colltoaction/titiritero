﻿namespace Fiuba.Algo3.Ejemplos.Pelotas.Vista
{
    using Fiuba.Algo3.Titiritero;
    using Fiuba.Algo3.Titiritero.Vista;

    public class VistaPelota2 : Imagen
    {
        public VistaPelota2(IPosicionable posicionable) : base(posicionable,"Recursos\\pelota.jpg")
        {

        }
    }
}
