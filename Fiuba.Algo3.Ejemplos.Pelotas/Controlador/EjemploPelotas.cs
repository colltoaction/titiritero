﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fiuba.Algo3.Titiritero;
using Fiuba.Algo3.Ejemplos.Pelotas.Vista;

namespace Fiuba.Algo3.Ejemplos.Pelotas
{
    public class EjemploPelotas
    {
        private ISuperficieDeDibujo superficieDeDibujo;
        private ControladorJuego controladorJuego;

        public EjemploPelotas(ISuperficieDeDibujo superficieDeDibujo)
        {
            this.superficieDeDibujo = superficieDeDibujo;
            this.controladorJuego = new ControladorJuego(superficieDeDibujo);
            this.controladorJuego.IntervaloSimulacion = 20;
            Mesa unaMesa = new Mesa();
            Pelota unaPelota = new Pelota(15, 15, unaMesa);
            Pelota otraPelota = new Pelota(60, 60, unaMesa);

            VistaMesa vistaMesa = new VistaMesa(unaMesa);
            VistaPelota vistaPelota = new VistaPelota(unaPelota);
            VistaPelota2 vistaOtraPelota = new VistaPelota2(otraPelota);

            controladorJuego.AgregarObjetoVivo(unaPelota);
            controladorJuego.AgregarObjetoVivo(otraPelota);

            // ojo al agregar los dibujables, siempre agregar primero los que van al fondo y luego los que van adelante
            controladorJuego.AgregarDibujable(vistaMesa);
            controladorJuego.AgregarDibujable(vistaPelota);
            controladorJuego.AgregarDibujable(vistaOtraPelota);
            
        }

        public void Comenzar()
        {
            this.controladorJuego.ComenzarJuego();
        }


        public void Detener()
        {
            this.controladorJuego.DetenerJuego();
        }
    }
}
