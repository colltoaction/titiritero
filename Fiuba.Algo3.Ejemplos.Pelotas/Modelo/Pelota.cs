﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fiuba.Algo3.Titiritero;

namespace Fiuba.Algo3.Ejemplos.Pelotas
{
    public class Pelota : IPosicionable, IObjetoVivo
    {

        public int X
        {
            get;
            private set;
        }

        public int Y
        {
            get;
            private set;
        }

        public Mesa LaMesa
        {
            get;
            set;
        }

        private int sentidoX;
        private int sentidoY;
        private int cantidadDeChoques;

        public Pelota()
        {
            this.sentidoX = 1;
            this.sentidoY = 1;
            this.X = 20;
            this.Y = 20;
        }

        public Pelota(int xInicial, int yInicial, Mesa mesa)
        {
            this.LaMesa = mesa;
            if (xInicial < 0)
            {
                this.sentidoX = -1;
                this.X = Math.Abs(xInicial);
            }
            if (yInicial < 0)
            {
                this.sentidoY = -1;
                this.Y = Math.Abs(yInicial);
            }
            this.sentidoX = 1;
            this.sentidoY = 1;
            this.X = xInicial;
            this.Y = yInicial;
        }

        public void Vivir()
        {
            this.X += this.sentidoX;
            this.X += this.sentidoX;
            this.Y += this.sentidoY;
            if (this.X >= this.LaMesa.Ancho || this.X < 0)
            {
                this.sentidoX = -1 * this.sentidoX;
                this.cantidadDeChoques++;
            }
            if (this.Y >= this.LaMesa.Alto || this.Y < 0)
            {
                this.sentidoY = -1 * this.sentidoY;
                this.cantidadDeChoques++;
            }

            /*
            Iterator<Obstaculo> iteradorDeObstaculos = mesa.iterator();
        while(iteradorDeObstaculos.hasNext()){
            Obstaculo otroObstaculo = iteradorDeObstaculos.next();
            if(!this.equals(otroObstaculo)){
                if(distancia(otroObstaculo)<5){
                    if( distancia (this.getX(),otroObstaculo.getX()) < 2){
                        this.sentidoX = -1 * this.sentidoX;
                        this.cantidadDeChoques++;
                    }
					
                    if( distancia (this.getY(),otroObstaculo.getY()) < 2){
                        this.sentidoY = -1 * this.sentidoY;
                        this.cantidadDeChoques++;
                    }
                }
            }
             * */
        }


        /*
    private double Distancia(Obstaculo obstaculo){
        double x = this.getX() - obstaculo.getX();
        double y = this.getY() - obstaculo.getY();
        return Math.sqrt( (x*x) + (y*y));
    }
        
    private int distancia(int punto1, int punto2){
        return Math.abs(punto1 - punto2);
    }
        */

        public int getCantidadDeChoques()
        {
            return this.cantidadDeChoques;
        }
    }
}