﻿namespace Fiuba.Algo3.Ejemplos.Pelotas
{
    using Fiuba.Algo3.Titiritero;

    public class Mesa : IPosicionable
    {
        public int X
        {
            get { return 0;} 
            
        }

        public int Y
        {
            get { return 0; }
        }


        public int Ancho { get; set; }

        public int Alto { get; set; }

        public Mesa()
        {
            this.Alto = 200;
            this.Ancho = 200;
        }
    }
}
