﻿namespace Fiuba.Algo3.Titiritero.Tests
{
    using System.Threading;
    using Moq;
    using NUnit.Framework;
    
    [TestFixture]
    public class ControladorJuegoTest
    {
        [Test]
        public void DeberiaLlamarVivir()
        {
            var superficieDeDibujoMockery = new Mock<ISuperficieDeDibujo>();
            ControladorJuego controladorJuego = new ControladorJuego(superficieDeDibujoMockery.Object);
            var objetoVivoMockery = new Mock<IObjetoVivo>();                
            controladorJuego.AgregarObjetoVivo(objetoVivoMockery.Object);
            controladorJuego.ComenzarJuego();
            Thread.Sleep(1000);
            controladorJuego.DetenerJuego();
            objetoVivoMockery.Verify(obj => obj.Vivir());
        }
    }
}
