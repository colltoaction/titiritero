Fork de [titiritero](https://code.google.com/p/titiritero/) en su revisión 123.

* * *

Este framework está basado en .NET 4.0.
Ha sido desarrollado utilizando las siguientes herramientas:

- Visual Studio 2010: codificación
- Nant (vnant-0.91-alpha2): herramienta de build
- Nunit (v2.5.9.10348): framework de prueba automatizada
- Moq (v4.0.10827): framework de mocking

Para que la configuración de build funcione es necesario:

- agregar nant
- definir una variable de entorno llamada NUNIT\_PATH que apunte al directorio donde se encuentra el ejecutable de unit

Nico Paez.