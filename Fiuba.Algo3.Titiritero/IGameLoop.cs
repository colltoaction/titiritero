﻿using System;
namespace Fiuba.Algo3.Titiritero
{
    public interface IGameLoop
    {
        void AgregarDibujable(IDibujable unDibujable);
        void AgregarObjetoVivo(IObjetoVivo objetoVivo);
        void ComenzarJuego();
        void DetenerJuego();
        void RemoverDibujable(IDibujable unDibujable);
        void RemoverObjetoVivo(IObjetoVivo objetoVivo);
    }
}
