﻿namespace Fiuba.Algo3.Titiritero
{
    public interface IDibujable
    {
        void Dibujar(ISuperficieDeDibujo superficieDeDibujo);
        IPosicionable Posicionable { get; set; }
    }
}
