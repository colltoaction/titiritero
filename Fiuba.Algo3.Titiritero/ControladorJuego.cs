﻿namespace Fiuba.Algo3.Titiritero
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Threading;

    public class ControladorJuego : Fiuba.Algo3.Titiritero.IGameLoop
    {
        public bool estaEnEjecucion;
        public double IntervaloSimulacion {get; set;}
        public ISuperficieDeDibujo superficieDeDibujo;
        private IList<IObjetoVivo> objetosVivos;
        private IList<IDibujable> dibujables;

        public ControladorJuego(ISuperficieDeDibujo superficieDeDibujo)
        {
            this.superficieDeDibujo = superficieDeDibujo;
            this.estaEnEjecucion = false;
            this.IntervaloSimulacion = 2000;
            this.objetosVivos = new List<IObjetoVivo>();
            this.dibujables= new List<IDibujable>();
        }

        public void ComenzarJuego()
        {
            BackgroundWorker delayer = new BackgroundWorker();

            delayer.DoWork += new DoWorkEventHandler(this.DelayerDoWork);
            delayer.RunWorkerAsync();

        }

        void DelayerDoWork(object sender, DoWorkEventArgs e)
        {
            this.ComenzarAsync();
        }

        public void ComenzarAsync()
        {
            estaEnEjecucion = true;
            while (estaEnEjecucion)
            {
                this.Simular();
                this.Dibujar();
                Thread.Sleep(TimeSpan.FromMilliseconds(this.IntervaloSimulacion));
            }
        }

        public void DetenerJuego()
        {
            this.estaEnEjecucion = false;
        }
	
        public void AgregarObjetoVivo(IObjetoVivo objetoVivo)
        {
            objetosVivos.Add(objetoVivo);
        }

        public void RemoverObjetoVivo(IObjetoVivo objetoVivo)
        {
            objetosVivos.Remove(objetoVivo);
        }

        public void AgregarDibujable(IDibujable unDibujable)
        {
            dibujables.Add(unDibujable);
        }

        public void RemoverDibujable(IDibujable unDibujable)
        {
            dibujables.Remove(unDibujable);
        }

        private void Dibujar()
        {
            this.superficieDeDibujo.Limpiar();
            foreach (IDibujable dibujable in this.dibujables)
            {
                dibujable.Dibujar(this.superficieDeDibujo);
            }
            this.superficieDeDibujo.Actualizar();
        }

        /**
         * Ejecuta la simulacion recorriendo la coleccion de objetivos vivos.
         */
        private void Simular()
        {
            foreach (IObjetoVivo objetoVivo in this.objetosVivos)
            {
                objetoVivo.Vivir();
            }
        }

    }
}
