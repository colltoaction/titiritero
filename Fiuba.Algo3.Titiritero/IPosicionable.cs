﻿namespace Fiuba.Algo3.Titiritero
{
    public interface IPosicionable
    {
        int X {get;}

        int Y { get; }
    }
}
