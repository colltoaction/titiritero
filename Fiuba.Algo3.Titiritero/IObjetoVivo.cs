﻿namespace Fiuba.Algo3.Titiritero
{
    public interface IObjetoVivo
    {
        void Vivir();
    }
}
