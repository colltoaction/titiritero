﻿namespace Fiuba.Algo3.Titiritero
{
    public interface ISuperficieDeDibujo
    {
        void Actualizar();
        void Limpiar();
        object Buffer { get; }
    }
}
