﻿namespace Fiuba.Algo3.Titiritero.Vista
{
    public abstract class Figura : IDibujable    
    {
        public Figura(System.Drawing.Color color)
        {
            this.Color = color;
        }

        public abstract void Dibujar(ISuperficieDeDibujo superficieDeDibujo);

        public IPosicionable Posicionable
        {
            get; set;
        }

        public System.Drawing.Color Color
        {
            set;
            get;
        }
    }
}
