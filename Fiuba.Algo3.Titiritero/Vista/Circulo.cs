﻿namespace Fiuba.Algo3.Titiritero.Vista
{
    using System.Drawing;

    public class Circulo : Figura
    {
        private int radio;

        public Circulo(int radio, System.Drawing.Color color): base(color)
        {
            this.radio = radio;
        }

        public override void Dibujar(ISuperficieDeDibujo superficieDeDibujo)
        {
            Graphics grafico = (Graphics)superficieDeDibujo.Buffer;
            using (SolidBrush brocha = new SolidBrush(this.Color))
            {
                grafico.FillEllipse(brocha, this.Posicionable.X, this.Posicionable.Y, this.radio, this.radio);
            }
        }

    }
}
