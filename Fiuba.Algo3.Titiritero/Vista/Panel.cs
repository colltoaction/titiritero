﻿namespace Fiuba.Algo3.Titiritero.Vista
{
    using System;
    using System.Drawing;

    public class Panel : System.Windows.Forms.Panel, ISuperficieDeDibujo
    {
        Bitmap bufferBmp;
        Graphics bufferG;

        public Panel()
        {
            bufferBmp = new Bitmap(this.Width, this.Height);
            bufferG = Graphics.FromImage(bufferBmp);
        }

        public Panel(int alto, int ancho)
        {
            this.Height = alto;
            this.Width = ancho;
            bufferBmp = new Bitmap(this.Height, this.Width);
            bufferG = Graphics.FromImage(bufferBmp);
        }

        public void Limpiar()
        {
            bufferG.FillRectangle(new SolidBrush(Color.Blue), this.ClientRectangle);
        }

        public void Actualizar()
        {
            Graphics g = this.CreateGraphics();
            g.DrawImage(bufferBmp, 0, 0);
        }

        public Object Buffer
        {
            get
            {
                return bufferG;
            }
        }

        protected override void OnResize(EventArgs eventargs)
        {
            bufferBmp = new Bitmap(this.Width, this.Height);
            bufferG = Graphics.FromImage(bufferBmp);
        }
    }
}