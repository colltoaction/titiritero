﻿namespace Fiuba.Algo3.Titiritero.Vista
{
    using System.Drawing;

    public class Imagen : IDibujable
    {
        private Image imagen;

        public IPosicionable Posicionable
        {
            get;
            set;
        }

        public Imagen(IPosicionable posicionable, string nombreArchivo)
        {
            this.Posicionable = posicionable;
            this.imagen = Image.FromFile(nombreArchivo);
        }

        public void Dibujar(ISuperficieDeDibujo superficieDeDibujo)
        {
            Graphics grafico = (Graphics)superficieDeDibujo.Buffer;
            grafico.DrawImage(imagen, this.Posicionable.X, this.Posicionable.Y);
        }
    }
}
