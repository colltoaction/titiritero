﻿namespace Fiuba.Algo3.Titiritero.Vista
{
    using System.Drawing;

    public class Cuadrado : Figura
    {
        private int ancho;
        private int alto;

        public Cuadrado(int ancho, int alto,System.Drawing.Color color): base(color)
        {
            this.alto = alto;
            this.ancho = ancho;
        }

        public override void Dibujar(ISuperficieDeDibujo superficieDeDibujo)
        {
            Graphics grafico = (Graphics)superficieDeDibujo.Buffer;
            using (SolidBrush brocha = new SolidBrush(this.Color))
            {
                grafico.FillRectangle(brocha, this.Posicionable.X, this.Posicionable.Y, this.ancho, this.alto);
            }
        }

    }
}
